import { Component, OnInit, OnDestroy } from '@angular/core';
import { BlogService } from '../services/blog.services';
import { Post } from '../shared/post.model';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css', '../shared/styles.css']
})
export class BlogListComponent implements OnInit, OnDestroy {
  posts = [];
  // Error is any, while fetching all posts
  error = "";
  isLoading = false;
  // The post selected in order to view the details
  postSelected : Post = null;
  // The index of the selected post that is being viewed
  selectedPostIndex;

  // Inject service
  constructor(private blogService : BlogService,
    private router : Router) { }

  ngOnInit(): void {
    this.getAllPosts();
    // Subscribe to "postUpdated" observable
    this.blogService.postEdited.subscribe(
      (post) => {
        // The post has changed, update "postSelected"
        this.postSelected.title = post.title;
        this.postSelected.content = post.content;
        this.postSelected.time = post.time;
        // Also update the element in the "posts" array.
        const postElementUpdated = {
          title : post.title,
          content : post.content,
          time : post.time,
          id : post.id
        }
        this.posts[this.selectedPostIndex] = postElementUpdated;
      }
    );
  }

  ngOnDestroy() {
    //this.blogService.postEdited.unsubscribe();
  }

  // Get all the posts
  getAllPosts() {
    this.isLoading = true;
    this.error = "";
    this.blogService.getAllPosts()
    .subscribe(
      (postsArr) => {
        this.posts = postsArr;
        this.isLoading = false;
      },
      (errors) => {
        this.error = "An error occured while loading posts !";
        this.isLoading = false;
      }
    );
  }

  // Invoked when a post is clicked
  onSelectPost(post, currentPostIndex : number) {
    let mySelectedPost : Post = new Post();
    mySelectedPost.id = post.id;
    mySelectedPost.title = post.title;
    mySelectedPost.content = post.content;
    mySelectedPost.time = post.time;
    this.postSelected = mySelectedPost;
    this.selectedPostIndex = currentPostIndex;
  }

  // To display the message
  isDeletingPost = false;

  // Invoked when "Delete" is clicked.
  onDeletePosts(myForm : NgForm) {
    // Determine the posts selected for deletion
    let postIdsToDelete = [];
    let val;
    for(let key in myForm.value) {
      val = myForm.value[key];
      if(val) {
        postIdsToDelete.push(key);
      }
    }
    console.log("postIdsToDelete : " + postIdsToDelete);
    if(postIdsToDelete.length > 0) {
      if(!confirm("Do you really want to delete the posts ?")) {
        return;
      }
      this.isDeletingPost = true;
      this.error = "";
      /*
      this.blogService.deletePosts(postIdsToDelete)
      .subscribe(
        (responseArr) => {
          this.isDeletingPost = false;
          this.error = "";
          // Now Refresh to display the current content
          this.router.navigate(["/readBlogs"]);
        },
        (errors) => {
          this.isDeletingPost = false;
          this.error = "An error occured while deleting posts !";
        }
      );
      */
     // Delete the posts
     this.blogService.deletePosts(postIdsToDelete);

      // Now Refresh to display the current content, after a delay
      setTimeout(() => {
        this.isDeletingPost = false;
        this.error = "";
        this.getAllPosts();
      }, 3000);
    }
  }

  // Calculates the background colour of a post based on even & odd row,
  // selected post
  getBackgroundColorClass(currentPost, currentIndex : number) {
    let bgColorClass = "";

    // Check if current post is selected
    if((this.postSelected != null) && (this.postSelected.id == currentPost.id)) {
      bgColorClass = "selectedPost";
    }

    // If current post is not selected, get class for even and odd rows
    if(bgColorClass == "") {
      if((currentIndex%2==0)) {
        bgColorClass = "evenPost";
      } else {
        bgColorClass = "oddPost";
      }
    }

    return bgColorClass;
  }
}
