import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogCreateComponent } from './blog-create/blog-create.component';
import { BlogListComponent } from './blog-list/blog-list.component';


const routes: Routes = [
  {path:"addPost", component: BlogCreateComponent},
  {path:"readPosts", component: BlogListComponent},
  {path:"", redirectTo:"/readPosts", pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
