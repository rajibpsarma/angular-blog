import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {map} from "rxjs/operators";

import {environment} from "src/environments/environment";
import { Post } from '../shared/post.model';
import { Subject, forkJoin } from 'rxjs';

@Injectable({providedIn:"root"})
export class BlogService {
  constructor(private http : HttpClient) {}
  // An observable to update the post edited
  postEdited = new Subject<Post>();

  // Adds a post in Firebase
  addPost(title:string, content:string) {
    const data = {
      title : title,
      content : content,
      time : new Date()
    }
    return this.http.post(environment.addPostUrl, data);
  }

  // Returns all the posts
  getAllPosts() {
    return this.http.get(environment.getPostsUrl)
    .pipe(map(
      (postsData) =>  {
        // postsData is in format {"some key':{our object}}
        // convert it into an array
        let postsArr = [];
        for(let key in postsData) {
            postsArr.push({...postsData[key], id:key});
        }
        return postsArr;
      }
    ))
  }

  updatePost(post) {
    let url = environment.updatePostUrl + "/" + post.id + ".json";
    const data = {
      title : post.title,
      content : post.content,
      time : new Date()
    }
    return this.http.patch(url, data);
  }

  deletePosts(postIdsToDelete) {
    let responseArr = [];
    let url = "";
    for(let postId in postIdsToDelete) {
      url = environment.updatePostUrl + "/" + postIdsToDelete[postId] + ".json";
      this.http.delete(url).subscribe(
        (restData) => {
          //console.log(restData);
        },
        (errors) => {
          //console.log(errors);
        }
      );
    }
  }
}
