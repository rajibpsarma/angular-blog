import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../shared/post.model';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {
  // Custom property
  @Input() selectedPost : Post;
  // flag to indicate if the post is being editted
  isPostEdited = false;

  constructor() { }

  ngOnInit(): void {
  }

  editPost() {
    this.isPostEdited = true;
  }
}
