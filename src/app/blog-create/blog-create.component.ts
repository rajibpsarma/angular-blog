import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BlogService } from '../services/blog.services';

@Component({
  selector: 'app-blog-create',
  templateUrl: './blog-create.component.html',
  styleUrls: ['./blog-create.component.css']
})
export class BlogCreateComponent implements OnInit {

  constructor(private blogService : BlogService) { }

  ngOnInit(): void {
  }

  error = "";
  postAdded = false;

  onSubmit(myForm: NgForm) {
    this.error = "";
    this.blogService.addPost(myForm.value.title, myForm.value.content)
    .subscribe(
      (restData) => {
        // success
        this.postAdded = true;
        myForm.reset();
      },
      (errors) => {
        this.error = errors.message;
      },
      () => {}
    );
  }
}
