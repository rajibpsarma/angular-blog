import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BlogService } from '../services/blog.services';
import { tap } from "rxjs/operators";
import { Post } from '../shared/post.model';

@Component({
  selector: 'app-blog-edit',
  templateUrl: './blog-edit.component.html',
  styleUrls: ['./blog-edit.component.css']
})
export class BlogEditComponent implements OnInit, OnChanges {
  // The selected Post
  @Input() postToEdit;
  isUpdating = false;
  isPostUpdated = false;
  error = "";
  // Set when "cancel" is clicked
  isCancelled = false;
  constructor(private blogService : BlogService) { }

  ngOnInit(): void {
  }

  // Triggers when any property changes
  ngOnChanges(changes: SimpleChanges) {
    if(changes.postToEdit != null) {
      // postToEdit has changed, set various status to their default values
      this.setStatusToDefault();
    }
  }

  onSubmit(myForm: NgForm) {
    this.setStatusToDefault();
    const data = {
      id : this.postToEdit.id,
      title : myForm.value.title,
      content : myForm.value.content
    }
    this.blogService.updatePost(data)
    .pipe(
      tap(
        (restData : {title,content,time}) => {
          // Post is updated in server, update the selected post in the post list, UI
          let post = new Post();
          post.title = restData.title;
          post.content = restData.content;
          post.time = restData.time;
          this.blogService.postEdited.next(post);
        }
      )
    )
    .subscribe(
      (restData : {title,content,time}) => {
        // success.
        this.isUpdating = false;
        this.isPostUpdated = true;
        myForm.reset();
      },
      (errors) => {
        this.error = "An error occurred while updating the post !";
        this.isUpdating = false;
        this.isPostUpdated = false;
      },
      () => {}
    );
  }

  cancel() {
    this.isCancelled = true;
  }

  setStatusToDefault() {
    this.isUpdating = false;
    this.isPostUpdated = false;
    this.isCancelled = false;
    this.error = "";
  }
}
