export const environment = {
  production: true,
  getPostsUrl: "https://blog-8f73c.firebaseio.com/blog-8f73c.json",
  addPostUrl: "https://blog-8f73c.firebaseio.com/blog-8f73c.json",
  updatePostUrl: "https://blog-8f73c.firebaseio.com/blog-8f73c"
};
