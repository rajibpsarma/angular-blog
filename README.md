# Blog

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.5.

It's a blog app that loads existing posts from a firebase database.
It also allows to add new posts, read the details of a post, edit a post and delete posts.

## Screenshot ##
![Screenshot of the Blog App](src/assets/images/screenshot.png)

## The steps to be followed are: ##
* ng new angular-blog
* npm install bootstrap --save
* ng serve
* Now explore the blog using "http://localhost:4200/"

